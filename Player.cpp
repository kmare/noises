/*
 * Player.cpp
 *
 *  Created on: Mar 22, 2019
 *      Author: kmare
 */

#include "Player.h"

Player::Player() {
	Player::init();
}

void Player::init() {
	DDRB |= (1<<PORTB1);
	TCCR1A |= (1<<COM1A1) | (1<<WGM10);  // setting PWM
	TCCR1B |= (1<<CS10) | (1<<WGM12);  // no prescaler, CTC mode

	// set timer0 to make sounds non-blocking
	TCCR0B |= (1 << CS01); // |(1 << CS00);
	TCNT0 = 0;

	// setup LED as output
	DDRB |= (1 << DDB4);
}

void Player::play(const unsigned char* data, int16_t size) {
	if (TCNT0 >= 180 && Player::isPlaying) {  // delay 90 μs
		OCR1A = (pgm_read_byte(&data[Player::current]));
		TCNT0 = 0;
		LED_ON;
		Player::current++;
		if (Player::current >= size) {
			Player::current = 0;
			Player::isPlaying = false;
			LED_OFF;
		}
	}
}

void Player::stop() {
	TCNT0 = 0;
	Player::current = 0;
	Player::isPlaying = false;
}

Player::~Player() {

}
