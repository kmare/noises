/*
 * main.cpp
 *
 *  Created on: Mar 22, 2019
 *      Author: kmare
 */

#include <stdlib.h>
#include "Player.h"
#include "data.h"

#include <avr/interrupt.h>

#define BUTTON_PRESSED (0 == (PINB & (1<<PORTB2)))

#define SIZE(a) (sizeof(a) / sizeof(*a))

unsigned char currentButtonState = 0, previousButtonState = 0; // button states

volatile uint8_t tot_overflow;

void init() {
	TCCR2B |= (1 << CS22)|(1 << CS21)|(1 << CS20);
	TCNT2 = 0;
	TIMSK2 |= (1 << TOIE2);
	sei();
	tot_overflow = 0;
}

ISR(TIMER2_OVF_vect) {
    tot_overflow++;
}

int main (void) {

	init();

	// button stuff
	DDRB &= ~(1 << DDB2);
	PORTB |= (1 << DDB2);

	unsigned char r = 0;

	Player player;
	const unsigned char *sounds[3];
	sounds[0] = live_long;
	sounds[1] = illogical;
	sounds[2] = logical;

	int16_t len[] = {SIZE(live_long), SIZE(illogical), SIZE(logical)};

	while(1) {
		currentButtonState = BUTTON_PRESSED;
		// if 30 ms have passed and if the button was pressed
		if ((tot_overflow>=3) && (currentButtonState != 0 && previousButtonState == 0)) {
			r = rand() % 3;
			player.stop();
			player.isPlaying = true;
			tot_overflow = 0;  // over 30 ms have passed - reset
		}

		player.play(sounds[r], len[r]);

		previousButtonState = currentButtonState;
	}
}
