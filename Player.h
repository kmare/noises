/*
 * Player.h
 *
 *  Created on: Mar 22, 2019
 *      Author: kmare
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#ifndef PLAYER_H_
#define PLAYER_H_

#ifdef F_CPU
#undef F_CPU
#define F_CPU 16000000UL
#endif

#define LED_ON  PORTB |= 1<<PORTB4
#define LED_OFF  PORTB &= ~(1<<PORTB4)

class Player {
public:
	Player();
	~Player();
public:
	int16_t current = 0;
	int16_t size = 0;
	bool isPlaying = false;
	void play(const unsigned char *data, int16_t size);
	void stop();
private:
	void init();
};

#endif /* PLAYER_H_ */
